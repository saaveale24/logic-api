﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace API.Models
{
    [DataContract]
    public class Horario
    {
        private SqlConnection con;
        private SqlCommand scmd;
        private SqlDataReader dr;
        private string cadenaP;

        public Horario()
        {
            #region Cadenas de conexion a BD
            cadenaP = ConfigurationManager.ConnectionStrings["ConnectionP"].ConnectionString;
            #endregion
        }
        [DataMember]
        public DateTime horario;

        
        public List<Horario> getHorario()
        {
            con = new SqlConnection(cadenaP);
            scmd = new SqlCommand("PA_HorarioSelect", con);
            scmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            dr = scmd.ExecuteReader();
            List<Horario> lresult = new List<Horario>();
            List<String> lcol = ModeloHelper.GetColumnInSP(dr);
            while (dr.Read())
            {
                if (lcol.Contains("DateTimeInterval") && !dr.IsDBNull(dr.GetOrdinal("DateTimeInterval")))
                {
                    Horario hr = new Horario();
                    hr.horario = dr.GetDateTime(dr.GetOrdinal("DateTimeInterval"));
                    lresult.Add(hr);
                }
            }
            dr.Close();
            con.Close();
            return lresult;
        }

    }
}
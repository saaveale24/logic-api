﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace API.Models
{
    public static class ModeloHelper
    {
        public static List<String> GetColumnInSP(SqlDataReader dr)
        {
            List<String> l = new List<String>();
            for(int col=0;col< dr.FieldCount; col++)
            {
                l.Add(dr.GetName(col).ToString());
            }
            return l;
        }
    }
}
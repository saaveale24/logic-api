﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace API.Models
{
    [DataContract]
    public class Cita
    {
        private SqlConnection con;
        private SqlCommand scmd;
        private SqlDataReader dr;
        private string cadenaP;

        public Cita()
        {
            #region Cadenas de conexion a BD
            cadenaP = ConfigurationManager.ConnectionStrings["ConnectionP"].ConnectionString;
            #endregion
        }
        [DataMember]
        public int codcita;
        [DataMember]
        public string placa;
        [DataMember]
        public DateTime fecha;
        [DataMember]
        public DateTime fhracrea;
        [DataMember]
        public DateTime fhramod;
        [DataMember]
        public int codestado;
        [DataMember]
        public string estado;
        [DataMember]
        public String ftexto;

        public void setCita(int Opcion, Cita cita)
        {
            con = new SqlConnection(cadenaP);
            scmd = new SqlCommand("PA_Cita", con);
            scmd.CommandType = CommandType.StoredProcedure;
            scmd.Parameters.AddWithValue("@Opcion", Opcion);
            scmd.Parameters.AddWithValue("@Placa", cita.placa);
            scmd.Parameters.AddWithValue("@Fecha", cita.ftexto);
            scmd.Parameters.AddWithValue("@CodEstado", cita.codestado);
            con.Open();
            scmd.ExecuteNonQuery();
            con.Close();
        }

        public List<Cita> getCita(int Opcion,string Placa)
        {
            con = new SqlConnection(cadenaP);
            scmd = new SqlCommand("PA_CitaSelect", con);
            scmd.CommandType = CommandType.StoredProcedure;
            scmd.Parameters.AddWithValue("@Opcion", @Opcion);
            scmd.Parameters.AddWithValue("@Placa", @Placa);
            con.Open();
            dr = scmd.ExecuteReader();
            List<Cita> lresult = new List<Cita>();
            List<String> lcol = ModeloHelper.GetColumnInSP(dr);
            while (dr.Read())
            {
                Cita cita = new Cita();
                if (lcol.Contains("placa") && !dr.IsDBNull(dr.GetOrdinal("placa")))
                {
                    cita.placa = dr.GetString(dr.GetOrdinal("placa"));
                }
                if (lcol.Contains("fecha") && !dr.IsDBNull(dr.GetOrdinal("fecha")))
                {
                    cita.fecha = dr.GetDateTime(dr.GetOrdinal("fecha"));
                }
                if (lcol.Contains("estado") && !dr.IsDBNull(dr.GetOrdinal("estado")))
                {
                    cita.estado = dr.GetString(dr.GetOrdinal("estado"));
                }
                lresult.Add(cita);
            }
            dr.Close();
            con.Close();
            return lresult;
        }

    }
}
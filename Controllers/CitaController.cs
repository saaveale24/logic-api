﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using API.Models;
using Newtonsoft.Json;

namespace API.Controllers
{
    [System.Web.Http.RoutePrefix("Api/Cita")]
    public class CitaController : ApiController
    {
        public JsonRequestBehavior JsonRequestBehavior { get; private set; }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("All")]
        public List<Cita> GetAll()
        {
            try
            {
                Cita cita = new Cita();
                List<Cita> lcita = cita.getCita(2, "");
                return lcita;
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET api/citas/LOG234
        public IHttpActionResult Get(string id)
        {
            List<Cita> lcita = new List<Cita>();
            try
            {
                Cita cita = new Cita();
                lcita=cita.getCita(1, id);
                if (lcita.Count == 0)
                {
                    return NotFound();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return Ok(lcita);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Horario")]
        public List<Horario> GetHorario()
        {
            try
            {
                Horario hr = new Horario();
                List<Horario> lhorario = hr.getHorario();
                return lhorario;
            }
            catch (Exception)
            {
                throw;
            }
        }

        // POST api/citas
        public IHttpActionResult Post(Cita cita)
        {
            try
            {
                cita.setCita(1, cita);
            }
            catch (Exception)
            {
                throw;
            }

            return Ok(cita);
        }

        // PUT api/citas/5
        public void Put(int id, [FromBody] string value)
        {
        }


        // DELETE api/citas/5
        public void Delete(int id)
        {
        }
    }
}
